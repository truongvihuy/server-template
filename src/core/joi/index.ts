import JoiLib from 'joi';
import _ from 'lodash';

type JoiType = typeof JoiLib & {
  objectId: () => JoiLib.StringSchema;
  ethAddress: () => JoiLib.StringSchema;
  txHash: () => JoiLib.StringSchema;
};

const Joi: JoiType = _.merge(JoiLib, {
  objectId: () =>
    Joi.alternatives(
      Joi.string().regex(/^[0-9a-fA-F]{24}$/, 'valid mongo id'),
      Joi.object().keys({
        id: Joi.any(),
        _bsontype: Joi.allow('ObjectId'),
      }),
    ) as any as JoiLib.StringSchema,

  ethAddress: () =>
    JoiLib.string()
      .alphanum()
      .min(64)
      .max(66)
      .regex(/^0x[a-fA-F0-9]{64}$/, 'Tx hash')
      .lowercase()
      .error(new Error('must be a valid ethereum transaction hash')),

  txHash: () =>
    JoiLib.string()
      .alphanum()
      .min(64)
      .max(66)
      .regex(/^0x[a-fA-F0-9]{64}$/, 'Tx hash')
      .lowercase()
      .error(new Error('must be a valid ethereum transaction hash')),
});

export default Joi;
