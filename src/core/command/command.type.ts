export type ConfigCommand = {
  disabledChildProcess?: boolean;
  prefix?: string;
};
