import { exec } from 'child_process';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

import Command from './command';
import { ConfigCommand } from './command.type';

const defaultConfig: ConfigCommand = {
  disabledChildProcess: false,
  prefix: `yarn`,
};

export default class CommandFactory {
  private _commands: { [key: string]: any } = {};
  private config: ConfigCommand = defaultConfig;
  get argv(): any {
    return yargs;
  }

  public setConfig(config: ConfigCommand): void {
    Object.keys(config).map((key) => {
      this.config[key] = config[key];
    });
  }

  public applyCommand(commands: any[]): void {
    commands.forEach((cmdCst) => {
      this._commands[cmdCst.scriptName] = cmdCst;
    });
  }

  public checkCommand(key: string): boolean {
    return !!this._commands[key];
  }

  public creacteCommand(key: string): Command {
    const cmdCst = this._commands[key];
    return new cmdCst();
  }

  public execute(key: string, argv: any, options?: { childProcess?: boolean }): void | Promise<void> {
    const cmd = this.creacteCommand(key);
    cmd.setParams(argv);
    if (this.config.disabledChildProcess || !options.childProcess) {
      return cmd.execute();
    }

    const cmdString = `${this.config.prefix} ${cmd.cmdLine}`;

    return new Promise<void>((resolve, reject) => {
      exec(cmdString, (error, stdout, stderr) => {
        console.log(`+ CMD: ${cmdString.green}`);
        console.log('  stdout: ' + stdout.green);
        console.log('  stderr: ' + stderr.red);

        if (error) {
          reject(error);
        }

        resolve();
      });
    });
  }

  public async run() {
    this.setConfig({ disabledChildProcess: true });

    const bin = hideBin(process.argv);
    const argv = await this.getArgs(bin);

    const commandKey = argv._.find((key) => this.checkCommand(key as string)) as string;

    if (!commandKey) {
      this.showHelp([...bin, '--h']);
    }

    const argv2 = await this.getArgsCommand(commandKey, bin);

    try {
      await this.execute(commandKey, argv2);
    } catch (e) {
      console.log(`[Command.run]`, e);
    }

    process.exit(0);
  }

  private cmdYargs(bin: string | string[]) {
    let y = yargs(bin).usage('<command> [options]');
    Object.keys(this._commands).forEach((commandKey) => {
      y = y.command(commandKey, this._commands[commandKey].description);
    });
    return y;
  }

  public getArgs(bin: string | string[]) {
    return this.cmdYargs(bin).argv;
  }

  public showHelp(bin: string | string[]) {
    return this.cmdYargs(bin).help('h').alias('h', 'help').argv;
  }

  public getArgsCommand(commandKey: string, bin: string | string[]) {
    const cmdCtr = this._commands[commandKey];
    const options = cmdCtr.options;
    let y = yargs(bin).usage(`Usage: ${commandKey}, ${cmdCtr.description}`);

    Object.keys(options).forEach((key) => {
      y = y.option(key, options[key]);
    });

    return y.help('h').alias('h', 'help').version(false).argv;
  }
}
