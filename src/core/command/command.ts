import yargs from 'yargs';

export default abstract class Command<TParams = { [key: string]: any }> {
  public static readonly scriptName: string;
  public static readonly description: string;
  public static readonly options: { [key in string]: yargs.Options };

  private _params: TParams;

  get params(): TParams {
    if (!this._params) {
      this._params = {} as TParams;
      Object.keys(Command.options).forEach((key) => {
        if (typeof Command.options[key].default !== 'undefined') {
          this._params[key] = Command.options[key].default;
        }
      });
    }
    return this._params;
  }
  public setParams(params: TParams): void {
    this._params = params;
  }

  public get cmdLine(): string {
    let cmdString: string = `${Command.scriptName}`;
    this._params &&
      Object.keys(this._params).map((key) => {
        if (Command.options[key]) {
          const alias = Command.options[key].alias || key;
          cmdString += ` --${alias} ${this._params[key]}`;
        }
      });

    return cmdString;
  }

  public abstract execute(): void | Promise<void>;
}
