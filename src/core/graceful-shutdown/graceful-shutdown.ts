import { applyEventEmitter } from 'core/event';
import { ProcessEmitter } from './graceful-shutdown.emitter';

export default class GracefulShutDown {
  public static registerGracefulShutdown(fn: () => Promise<void> | void) {
    applyEventEmitter(process, ProcessEmitter, { params: [fn] });
  }
}
