import { Listener } from 'core/event';

export class ProcessEmitter {
  private exitCode = 0;
  private exit = false;

  constructor(private callback: () => Promise<void> | void) {
    this.exitCode = 0;
    this.exit = false;
  }

  private async handle(err?: Error) {
    if (this.exit) {
      process.exit(this.exitCode);
    } else {
      this.exit = true;
      try {
        await this.callback();
      } catch (e) {
        this.exitCode = 1;
      } finally {
      }
    }
  }

  @Listener('uncaughtException', { type: 'on' })
  public async uncaughtException(err: Error) {
    console.log(`[Process.uncaughtException]: `, err);
    this.exitCode = 1;
    await this.handle(err);
  }

  @Listener('SIGINT', { type: 'on' })
  public async SIGINT() {
    await this.handle();
  }

  @Listener('SIGTERM', { type: 'on' })
  public async SIGTERM() {
    await this.handle();
  }

  @Listener('SIGUSR2', { type: 'on' })
  public async SIGUSR2() {
    await this.handle();
  }
}
