import mongoose, { Connection, ConnectOptions, Model } from 'mongoose';

import { applyEventEmitter } from '../event';

export interface IConfigMongo {
  connectionString?: string;
  username?: string;
  password?: string;
  host?: string;
  port?: string | number;
  dbName?: string;
  emitter?: any;
}

export class MongodbService {
  private _connection: Connection | null | undefined;
  private _connectionString: string;
  private _options: ConnectOptions;
  private _emitter?: any;

  constructor(config: IConfigMongo | string, options: ConnectOptions | null = null) {
    if (typeof config === 'string') {
      this._connectionString = config;
    } else {
      if (config.connectionString) {
        this._connectionString = config.connectionString;
      } else {
        if (config.username) {
          this._connectionString = `mongodb://${encodeURIComponent(config.username)}:${encodeURIComponent(
            config.password ?? '',
          )}@${config.host}:${config.port}/${config.dbName}`;
        } else {
          this._connectionString = `mongodb://${config.host}:${config.port}/${config.dbName}`;
        }
      }
      this._emitter = config.emitter;
    }
    this._options = options ?? {};

    this.connect();
  }

  public connect(): Connection {
    if (
      !this._connection ||
      (this._connection && this._connection.readyState !== mongoose.ConnectionStates.connecting)
    ) {
      this._connection = mongoose.createConnection(this._connectionString, this._options);

      if (this._emitter) {
        applyEventEmitter(this._connection, this._emitter);
      }
    }

    return this._connection;
  }

  public async close(): Promise<void> {
    if (this._connection) {
      await this._connection.close();
      this._connection = null;
    }
  }

  public async syncIndexes(models: Model<any>[]): Promise<void> {
    for (const model of models) {
      try {
        await model.syncIndexes();
      } catch (error) {
        console.log(`[MongodbService.syncIndexes]:`, error);
      }
    }
  }
}
