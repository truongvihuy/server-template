import { CronJob } from 'cron';

export default abstract class Cron {
  private _cron: CronJob;
  private _cronName: string;
  private idle: boolean = true;

  constructor(cronName: string, cronTime: string | Date, timeZone?: string) {
    this._cron = new CronJob(cronTime, async () => await this.cronCommand(), null, false, timeZone);
    this._cronName = cronName;
    this.idle = true;
  }

  get cronName(): string {
    return this._cronName;
  }

  public start(): void {
    this._cron.start();
  }

  public stop(): void {
    this._cron.stop();
  }

  private async cronCommand(): Promise<void> {
    try {
      if (this.idle) {
        this.idle = false;
        await this._cronCommand();
      }
    } catch (e) {}

    this.idle = true;
  }

  protected abstract _cronCommand(): void | Promise<void>;
}
