import { getClassSchema, JoiSchema } from 'joi-class-decorators';

import Joi from 'core/joi';

export enum PaginatedType {
  total = 'total',
  hasMore = 'has-more',
}

export enum HttpStatus {
  Success = 200,
  BadRequest = 400,
  Unauthorized = 401,
  Forbidden = 403,
  NotFound = 404,
  Internal = 500,
}

export class PaginatedResponse<TItem> {
  @JoiSchema(Joi.array().required().description('List of data'))
  items: TItem[];

  @JoiSchema(Joi.boolean().optional().description('Flag has more data'))
  hasMore?: boolean;

  @JoiSchema(Joi.number().integer().optional().description('Total data list'))
  total?: number;

  @JoiSchema(Joi.number().integer().optional().description('Total page'))
  totalPage?: number;

  @JoiSchema(Joi.number().integer().optional().description('Current page'))
  currentPage?: number;
}

export const createJoiPaginatedResponse = (objectValidate: any) => {
  const joiPag = getClassSchema(PaginatedResponse);
  const joiItem = getClassSchema(objectValidate);
  joiPag.$_terms.keys[0].schema = joiItem;

  return joiPag;
};

export const createJoiArrayResponse = (objectValidate: any) => {
  const joiItem = getClassSchema(objectValidate);
  return Joi.array().items(joiItem);
};

export class PaginatedQuery {
  @JoiSchema(Joi.string().optional().description('Field data'))
  field?: string;

  @JoiSchema(Joi.number().default(0).optional().description('Skip in per request'))
  skip?: number;

  @JoiSchema(Joi.number().optional().description('Limit in per request'))
  limit?: number;

  @JoiSchema(Joi.number().default(1).optional().description('Page number'))
  page?: number;

  @JoiSchema(Joi.string().optional().description('Field sort'))
  sort?: string;
}
