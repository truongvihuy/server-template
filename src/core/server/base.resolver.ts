import _ from 'lodash';
import { FilterQuery, Model, mongo, PipelineStage, QueryOptions, UpdateQuery } from 'mongoose';

import { PaginatedResponse, PaginatedType } from './type';

class ResolverBase<T = {}> {
  private _model: Model<T>;

  constructor(model: Model<T>) {
    this._model = model;
  }

  get model() {
    return this._model;
  }

  public find(filter: FilterQuery<T> = {}, projection?: any, options: QueryOptions = { lean: true }) {
    return this._model.find(filter, projection, options);
  }

  public findOne(filter?: FilterQuery<T>, projection?: any, options: QueryOptions = { lean: true }) {
    return this._model.findOne(filter, projection, options);
  }

  public count(filter: FilterQuery<T> = {}) {
    return this._model.countDocuments(filter);
  }

  public distinct(field: string, filter: FilterQuery<T>) {
    return this._model.distinct(field, filter);
  }

  public async getPaginatedList(
    {
      filter = {},
      projection = null,
      options = { lean: true },
    }: { filter?: FilterQuery<T>; projection?: any; options?: QueryOptions },
    type: string = PaginatedType.hasMore,
  ): Promise<PaginatedResponse<T>> {
    if (type === PaginatedType.total) {
      const [items, total] = await Promise.all([this.find(filter, projection, options), this.count(filter)]);

      return {
        items,
        total,
        totalPage: options.limit ? Math.ceil(total / options.limit) : undefined,
        currentPage: options.limit ? Math.floor((options.skip || 0) / options.limit) + 1 : undefined,
      };
    }

    const optionsTmp = _.cloneDeep(options);

    if (optionsTmp.limit) {
      optionsTmp.limit++;
    }
    const response = {
      items: await this.find(filter, projection, optionsTmp),
      hasMore: false,
    };

    if (response.items.length === optionsTmp.limit) {
      response.items.pop();
      response.hasMore = true;
    }

    return response;
  }

  public async insertOne(payload: T): Promise<T> {
    // const item = new this._model(payload);
    // return (await item.save()).toJSON();
    // or
    const data = (await this._model.create(payload)) as any;
    return data.toJSON() as T;
  }

  public insertMany(payload: T[]) {
    return this._model.insertMany(payload);
  }

  public updateOne(
    filter?: FilterQuery<T>,
    update: UpdateQuery<T> = {},
    options: QueryOptions = {
      // https://mongoosejs.com/docs/api.html#model_Model.findOneAndUpdate
      new: true, // trả về dữ liệu mới nhất, nếu false trả về dữ liệu cũ
      lean: true, // nếu đúng sẽ trả dữ liệu Object, thay vì document mongoose
      // season: null, //
      // strict: true, // đảm bảo rằng các giá trị được chuyển đến hàm tạo mô hình của chúng tôi mà không được chỉ định trong lược đồ của chúng tôi sẽ không được lưu vào db.
      omitUndefined: true, // xóa tất cả các thuộc tính undefined
      // timestamps: null, //Nếu được đặt thành false và dấu thời gian cấp giản đồ được bật, hãy bỏ qua dấu thời gian cho bản cập nhật này. Lưu ý rằng điều này cho phép bạn ghi đè dấu thời gian. Không có gì nếu dấu thời gian cấp giản đồ không được đặt.
      // returnOriginal: null, // Một bí danh cho tùy chọn mới. returnOriginal: false tương đương với new: true.
      // overwrite: false, // Nếu đúng thay thể toàn bộ dữ liệu, các trường lúc trước có thể mất nếu dữ liệu mới không có trường đó
      // upsert: false, // nếu đúng và không tìm thấy tài liệu nào, hãy chèn một tài liệu mới
      // projection: null,
    },
  ) {
    return this._model.findOneAndUpdate(filter, update, options);
  }

  public updateMany(filter?: FilterQuery<T>, update: UpdateQuery<T> = {}, options?: mongo.UpdateOptions) {
    return this._model.updateMany(filter, update, options);
  }

  public deleteOne(filter?: FilterQuery<T>, options?: QueryOptions) {
    return this._model.findOneAndDelete(filter, options);
  }

  public deleteMany(filter?: FilterQuery<T>, options?: mongo.DeleteOptions) {
    return this._model.deleteMany(filter, options);
  }

  public aggregate(pipeline?: PipelineStage[]) {
    return this._model.aggregate(pipeline);
  }

  public bulkWrite(writes: any[], options?: any) {
    return this._model.bulkWrite(writes, options);
  }
}

export default ResolverBase;
