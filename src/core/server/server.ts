import { json, OptionsJson } from 'body-parser';
import corsMiddleware from 'cors';
import express, { Application, IRouter, Router } from 'express';
import { NextFunction, Request, Response } from 'express-serve-static-core';
import { getClassSchema } from 'joi-class-decorators';
import swaggerUI from 'swagger-ui-express';

import GracefulShutDown from 'core/graceful-shutdown';
import {
  ControllerDefinition,
  MIDDLEWARE_METADATA,
  MiddlewareDefinition,
  PARAMS_METADATA,
  ParamsDefinition,
  PATH_METADATA,
  RESPONSE_METADATA,
  ResponseDefinition,
  ROUTE_METADATA,
  RouteDefinition,
} from './decorators';
import Exception from './exception';
import DocumentSwagger from './swagger';
import { HttpStatus } from './type';

export type ServerOptionsConfig = {
  app: Application;
  document: boolean | string;
  swaggerDoc: DocumentSwagger;
  port: string | number;
  path: string;
  beforeShutdown: () => Promise<void> | void;
  middleware: ((...params: any[]) => void)[];
  handleResponse: (data: any, error: any, req: Request, res: Response, next: NextFunction) => void;
};

export type MiddlewareOptions = {
  path?: string;
  cors?: corsMiddleware.CorsOptions | corsMiddleware.CorsOptionsDelegate | boolean;
  bodyParserConfig?: OptionsJson | boolean;
  controllers: any[];
  middleware?: ((...params: any[]) => void)[];
  handleResponse?: (data: any, error: any, req: Request, res: Response, next: NextFunction) => void;
  handleError?: ((...params: any[]) => void)[];
};

export class Server {
  private app: Application;
  private port: string | number;
  private path: string;
  private document: string;
  private docSwagger: DocumentSwagger;
  private handleResponse:
    | undefined
    | ((data: any, error: any, req: Request, res: Response, next: NextFunction) => void);

  constructor(config: Partial<ServerOptionsConfig>) {
    // super();
    if (config.app) {
      this.app = config.app;
    } else {
      this.app = express();
    }
    this.port = config.port || 4000;
    this.path = config.path || '/';

    this.document = typeof config.document === 'boolean' ? config.document && '/documents' : config.document;

    if (config.middleware?.length) {
      this.app.use(config.middleware);
    }

    if (config.beforeShutdown) {
      GracefulShutDown.registerGracefulShutdown(config.beforeShutdown);
    }

    if (config.swaggerDoc) {
      this.docSwagger = config.swaggerDoc;
    }

    if (config.handleResponse) {
      this.handleResponse = config.handleResponse;
    }
  }

  public applyMiddleware(config: MiddlewareOptions) {
    this.app.use(config.path ?? this.path, this._getMiddleware(config));
    if (this.document) {
      this.docSwagger = this.docSwagger || new DocumentSwagger();
      this.app.use(
        this.document,
        swaggerUI.serve,
        swaggerUI.setup(this.docSwagger.genarateDocument(config.path ?? this.path, config.controllers)),
      );
    }
  }

  private _getMiddleware(config: MiddlewareOptions): IRouter {
    const router: IRouter = Router();

    if (config.cors === true) {
      router.use(corsMiddleware());
    } else if (config.cors !== false) {
      router.use(corsMiddleware(config.cors));
    }

    if (config.bodyParserConfig === true) {
      router.use(json());
    } else if (config.bodyParserConfig !== false) {
      router.use(json(config.bodyParserConfig));
    }

    if (config.middleware?.length) {
      router.use(config.middleware);
    }

    const handleResponse = config.handleResponse ?? this.handleResponse;

    config.controllers.forEach((controller) => {
      const metadataController: ControllerDefinition = Reflect.getMetadata(PATH_METADATA, controller);
      const routes: RouteDefinition[] = Reflect.getMetadata(ROUTE_METADATA, controller) ?? [];

      const instance = new controller();

      routes.forEach((route) => {
        const pipeParams: ParamsDefinition[] = Reflect.getMetadata(PARAMS_METADATA, controller, route.funcName) ?? [];
        const responseDef: ResponseDefinition =
          Reflect.getMetadata(RESPONSE_METADATA, controller, route.funcName) ?? null;
        const middlewares: MiddlewareDefinition[] =
          Reflect.getMetadata(MIDDLEWARE_METADATA, controller, route.funcName) ?? [];

        router[route.method](
          metadataController.prefix + route.path,
          ...(middlewares.map((mid) => mid.callback) as any[]),
          async (req: Request, res: Response, next: NextFunction) => {
            try {
              const params = await Promise.all(
                pipeParams.map((item: ParamsDefinition) => {
                  switch (item.paramType) {
                    case 'request': {
                      return req;
                    }
                    case 'response': {
                      return res;
                    }
                    case 'next': {
                      return next;
                    }
                    case 'callback': {
                      const data = item.callback(req, res, next);
                      if (item.objectValidate) {
                        const joiValidate = getClassSchema(item.objectValidate);
                        const { value, error } = joiValidate.validate(data);

                        if (error) {
                          throw error;
                        }
                        return value;
                      }
                      return data;
                    }
                    default: {
                      if (item.objectValidate) {
                        const joiValidate = getClassSchema(item.objectValidate);
                        const data = req[item.paramType];
                        const { value, error } = joiValidate.validate(data);

                        if (error) {
                          throw error;
                        }
                        return value;
                      }
                      return req[item.paramType];
                    }
                  }
                }),
              );

              // Todo: handle req
              let response = await (instance as any)[route.funcName](...params);

              // Todo: validate data
              if (responseDef?.joiValidate) {
                const { value, error } = responseDef.joiValidate.validate(response);
                if (error) {
                  throw error;
                }
                response = value;
              }

              if (handleResponse) {
                handleResponse(response, null, req, res, next);
              } else {
                if (!res.headersSent) {
                  res.send(response);
                }
              }
            } catch (error: any) {
              if (handleResponse) {
                handleResponse(null, error, req, res, next);
              } else {
                const _error = error as Error as Exception;
                _error.code = HttpStatus.Internal;
                next(_error);
              }
            }
          },
        );
      });
    });

    if (config.handleError) {
      router.use(config.handleError);
    }

    return router;
  }

  public use(...params: any[]) {
    this.app.use(...params);
  }

  public getApp() {
    return this.app;
  }

  public run(callback?: () => void | Promise<void>) {
    this.app.listen(this.port, callback);
  }
}
