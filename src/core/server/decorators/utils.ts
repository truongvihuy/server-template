import {
  ControllerDefinition,
  PARAMS_METADATA,
  PATH_METADATA,
  ParamsType,
  ROUTE_METADATA,
  RouteDefinition,
} from './constants';

export const getMetadataController = (target: Object) => {
  let metadata: ControllerDefinition;
  if (Reflect.hasMetadata(PATH_METADATA, target.constructor)) {
    metadata = Reflect.getMetadata(PATH_METADATA, target.constructor);
  }

  return metadata;
};

export const getMetadataMethod = (target: Object, propertyKey: string) => {
  let routes: RouteDefinition[] = [];
  if (Reflect.hasMetadata(ROUTE_METADATA, target.constructor)) {
    routes = Reflect.getMetadata(ROUTE_METADATA, target.constructor);
  }

  return routes.find((route) => route.funcName === propertyKey);
};

export const getIndexMetadataParams = (target: Object, propertyKey: string, paramType: ParamsType) => {
  let pipeParams: ParamsType[] = [];
  if (Reflect.hasMetadata(PARAMS_METADATA, target.constructor, propertyKey)) {
    pipeParams = Reflect.getMetadata(PARAMS_METADATA, target.constructor, propertyKey);
  }

  return pipeParams.findIndex((_paramType) => _paramType === paramType);
};
