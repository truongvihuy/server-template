import 'reflect-metadata';

import { ControllerDefinition, PATH_METADATA } from './constants';

export const Controller = (prefix: string = '/', tags: string[] = []): ClassDecorator => {
  // 'target' is Controller
  return (target) => {
    const metadata: ControllerDefinition = { prefix: ['', '/'].includes(prefix) ? '' : prefix, tags };
    // set 'prefix' route name
    Reflect.defineMetadata(PATH_METADATA, metadata, target);
  };
};
