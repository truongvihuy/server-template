import 'reflect-metadata';

import { MIDDLEWARE_METADATA, MiddlewareDefinition, MiddlewareFunc } from './constants';

export const createMiddlewareDecorator = (callback: MiddlewareFunc, metadata?: any): MethodDecorator => {
  // 'target' is Controller, 'propertyKey' is method name, descriptor is method
  return (target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<any>) => {
    let middlewares: MiddlewareDefinition[] = [];
    if (Reflect.hasMetadata(MIDDLEWARE_METADATA, target.constructor, propertyKey)) {
      middlewares = Reflect.getMetadata(MIDDLEWARE_METADATA, target.constructor, propertyKey);
    }

    middlewares.push({ callback, metadata });
    Reflect.defineMetadata(MIDDLEWARE_METADATA, middlewares, target.constructor, propertyKey as string);
  };
};
