import 'reflect-metadata';

import { PARAMS_METADATA, ParamsDefinition, ParamsFunc, ParamsType } from './constants';

const createPipesRouteParamDecorator =
  (paramType: ParamsType) =>
  (objectValidate?: Object): ParameterDecorator => {
    // 'target' is Controller, 'propertyKey' is method name, 'parameterIndex' is parameter index
    return (target, propertyKey, parameterIndex) => {
      let pipeParams: ParamsDefinition[] = [];
      if (Reflect.hasMetadata(PARAMS_METADATA, target.constructor, propertyKey as string)) {
        pipeParams = Reflect.getMetadata(PARAMS_METADATA, target.constructor, propertyKey as string);
      }

      pipeParams[parameterIndex] = {
        paramType: paramType,
        objectValidate,
      };
      Reflect.defineMetadata(PARAMS_METADATA, pipeParams, target.constructor, propertyKey as string);
    };
  };

export const Query = createPipesRouteParamDecorator('query');
export const Params = createPipesRouteParamDecorator('params');
export const Body = createPipesRouteParamDecorator('body');
export const RawHeaders = createPipesRouteParamDecorator('rawHeaders');
export const Headers = createPipesRouteParamDecorator('headers');
export const Cookies = createPipesRouteParamDecorator('cookies');
export const File = createPipesRouteParamDecorator('file');
export const Files = createPipesRouteParamDecorator('files');
export const Request = createPipesRouteParamDecorator('request')();
export const Response = createPipesRouteParamDecorator('response')();
export const Next = createPipesRouteParamDecorator('next')();

export const createParamCallbackDecorator = <T = any>(callback: ParamsFunc<T>): ParameterDecorator => {
  // 'target' is Controller, 'propertyKey' is method name, 'parameterIndex' is parameter index
  return (target, propertyKey, parameterIndex) => {
    let pipeParams: ParamsDefinition[] = [];
    if (Reflect.hasMetadata(PARAMS_METADATA, target.constructor, propertyKey as string)) {
      pipeParams = Reflect.getMetadata(PARAMS_METADATA, target.constructor, propertyKey as string);
    }

    pipeParams[parameterIndex] = {
      paramType: 'callback',
      callback,
    };
    Reflect.defineMetadata(PARAMS_METADATA, pipeParams, target.constructor, propertyKey as string);
  };
};
