import { NextFunction, Request, Response } from 'express-serve-static-core';
import Joi from 'joi';

export const PATH_METADATA = 'path';
export const ROUTE_METADATA = 'routes';
export const PARAMS_METADATA = 'params';
export const RESPONSE_METADATA = 'response';
export const MIDDLEWARE_METADATA = 'middleware';
export const SECURITY_METADATA = 'securities';

/** Controller */
export type ControllerDefinition = {
  prefix: string;
  tags: string[];
};

/** Request method */
export type MetadataDefinition = {
  description?: string;
  tags?: string[];
  // security?: any[];
};

export type RequestMethod = 'get' | 'post' | 'put' | 'delete' | 'options' | 'patch';
export type RouteDefinition = {
  method: RequestMethod;
  path: string;
  funcName: string | symbol;
  metadata?: MetadataDefinition;
};

/** Params */
export type ParamsRoot = 'request' | 'response' | 'next';
export type ParamsKey = 'query' | 'params' | 'body' | 'rawHeaders' | 'cookies' | 'headers' | 'files' | 'file';
export type ParamsFunc<T = any> = (req: Request, res: Response, next: NextFunction) => T;
export type MiddlewareFunc = (req: Request, res: Response, next: NextFunction) => void | Promise<void>;
export type ParamsType = ParamsRoot | ParamsKey | 'callback';
export type ParamsDefinition = {
  paramType: ParamsType;
  objectValidate?: any;
  callback?: ParamsFunc;
};
export type MiddlewareDefinition = {
  callback?: MiddlewareFunc;
  metadata?: any;
  options?: any;
};
export type ResponseDefinition = {
  joiValidate?: Joi.Schema;
  mediaTypes?: string[];
};

/** Securities */
export type SecurityScheme = 'basic' | 'bearer';
export type SecuritiesDefinition = {
  type: 'http' | 'apiKey' | 'openIdConnect';
  in?: 'header' | 'basic' | 'cookie';
  name?: string;
  scheme?: SecurityScheme;
  bearerFormat?: 'JWT';
  openIdConnectUrl?: string;
};

export type IFiles = {
  fieldname: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  destination: string;
  filename: string;
  path: string;
  size: number;
};
