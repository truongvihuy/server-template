import Joi from 'joi';
import 'reflect-metadata';

import { MetadataDefinition, RESPONSE_METADATA, ROUTE_METADATA, RequestMethod, RouteDefinition } from './constants';

const createMappingDecorator =
  (method: RequestMethod) =>
  (path: string = '/', metadata?: MetadataDefinition | string): MethodDecorator => {
    // 'target' is Controller, 'propertyKey' is method name, descriptor is method
    return (target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<any>) => {
      let routes: RouteDefinition[] = [];
      if (Reflect.hasMetadata(ROUTE_METADATA, target.constructor)) {
        routes = Reflect.getMetadata(ROUTE_METADATA, target.constructor);
      }

      if (typeof metadata === 'string') {
        metadata = { description: metadata };
      }

      routes.push({
        method,
        path: ['', '/'].includes(path) ? '' : path,
        funcName: propertyKey,
        metadata,
      });
      Reflect.defineMetadata(ROUTE_METADATA, routes, target.constructor);
    };
  };

export const Get = createMappingDecorator('get');
export const Post = createMappingDecorator('post');
export const Put = createMappingDecorator('put');
export const Delete = createMappingDecorator('delete');
export const Patch = createMappingDecorator('patch');

export const ValidateResponse = (options: {
  joiValidate?: Joi.Schema;
  mediaType?: string | string[];
}): MethodDecorator => {
  const mediaTypes = !options.mediaType
    ? ['application/json']
    : typeof options.mediaType === 'string'
    ? [options.mediaType]
    : options.mediaType;

  return (target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<any>) => {
    Reflect.defineMetadata(
      RESPONSE_METADATA,
      { joiValidate: options.joiValidate, mediaTypes },
      target.constructor,
      propertyKey,
    );
  };
};
