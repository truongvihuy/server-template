import { getClassSchema } from 'joi-class-decorators';
import joiToSwagger from 'joi-to-swagger';
import _ from 'lodash';
import { JsonObject } from 'swagger-ui-express';

import {
  ControllerDefinition,
  MIDDLEWARE_METADATA,
  MiddlewareDefinition,
  PARAMS_METADATA,
  ParamsDefinition,
  PATH_METADATA,
  RESPONSE_METADATA,
  ResponseDefinition,
  ROUTE_METADATA,
  RouteDefinition,
  SecuritiesDefinition,
  SECURITY_METADATA,
} from './decorators';

export default class DocumentSwagger {
  private _swaggerJson: JsonObject = {};
  private _formatDocResponse;

  constructor(doc?: JsonObject, formatDocResponse?: (response: any, error?: any) => any) {
    this._swaggerJson = _.merge(this._swaggerJson, doc);
    this._formatDocResponse = formatDocResponse;
  }
  //#region Document

  private initDocument = () => {
    this._swaggerJson = _.merge(this._swaggerJson ?? {}, {
      paths: {},
      components: {
        securitySchemes: {},
        schemas: {},
      },
    });
  };

  public genarateDocument(path, controllers: any[]): JsonObject {
    try {
      this.initDocument();
      controllers.forEach((controller) => {
        const metadataController: ControllerDefinition = Reflect.getMetadata(PATH_METADATA, controller);
        const routes: RouteDefinition[] = Reflect.getMetadata(ROUTE_METADATA, controller) ?? [];

        routes.forEach((route) => {
          /** Get metadata */
          const responseDef: ResponseDefinition = Reflect.getMetadata(
            RESPONSE_METADATA,
            controller,
            route.funcName as string,
          );
          const pipeParams: ParamsDefinition[] =
            Reflect.getMetadata(PARAMS_METADATA, controller, route.funcName as string) ?? [];
          const securities: SecuritiesDefinition[] =
            Reflect.getMetadata(SECURITY_METADATA, controller, route.funcName) ?? [];
          const middlewares: MiddlewareDefinition[] =
            Reflect.getMetadata(MIDDLEWARE_METADATA, controller, route.funcName) ?? [];

          /** Path */
          let fullPath = `${path}${metadataController.prefix}${route.path}`;
          const reg = /:(?<field>[^\/]+)/gm;
          const matches = fullPath.matchAll(reg);
          for (const m of matches) {
            fullPath = fullPath.replace(m[0], `{${m[1]}}`);
          }

          /** Tags */
          const tags = metadataController.tags;

          /** Sec, Params, Req Body, Response */
          let parameters: any[] = [];
          let security: any[] = [];
          let requestBody = {};
          let responses = {};

          pipeParams.forEach((eParam) => {
            switch (eParam.paramType) {
              case 'headers': {
                if (eParam.objectValidate) {
                  const { swagger } = joiToSwagger(getClassSchema(eParam.objectValidate));
                  Object.keys(swagger.properties).forEach((field) => {
                    parameters.push({
                      name: field,
                      in: 'headers',
                      description: swagger.properties[field].description ?? field,
                      schema: swagger.properties[field],
                      required: swagger.properties[field].required,
                    });
                  });
                }
                break;
              }
              case 'query': {
                if (eParam.objectValidate) {
                  const { swagger } = joiToSwagger(getClassSchema(eParam.objectValidate));
                  Object.keys(swagger.properties).forEach((field) => {
                    parameters.push({
                      name: field,
                      in: 'query',
                      description: swagger.properties[field].description ?? field,
                      schema: swagger.properties[field],
                      required: swagger.properties[field].required,
                    });
                  });
                }
                break;
              }
              case 'params': {
                if (eParam.objectValidate) {
                  const { swagger } = joiToSwagger(getClassSchema(eParam.objectValidate));
                  Object.keys(swagger.properties).forEach((field) => {
                    parameters.push({
                      name: field,
                      in: 'path',
                      description: swagger.properties[field].description ?? field,
                      schema: swagger.properties[field],
                      required: swagger.properties[field].required,
                    });
                  });
                }
                break;
              }
              case 'body': {
                if (eParam.objectValidate) {
                  const { swagger } = joiToSwagger(getClassSchema(eParam.objectValidate));
                  requestBody = {
                    // description: 'Payload data',
                    content: {
                      'application/json': {
                        schema: swagger,
                      },
                    },
                  };
                }
                break;
              }
            }
          });

          securities.forEach((eSe) => {
            security.push({ [eSe.name]: [] });
            this._swaggerJson.components.securitySchemes[eSe.name] = eSe;
          });

          middlewares.forEach((mi) => {
            if (mi.metadata) {
              mi.metadata.security && (security = _.merge(security, mi.metadata.security));
              mi.metadata.parameters && (parameters = _.merge(parameters, mi.metadata.parameters));
              mi.metadata.requestBody && (requestBody = _.merge(requestBody, mi.metadata.requestBody));
            }
          });

          if (responseDef) {
            const content = {};
            const { swagger } = responseDef.joiValidate ? joiToSwagger(responseDef.joiValidate) : { swagger: null };
            responseDef?.mediaTypes.forEach((mediaType) => {
              content[mediaType] = {
                schema: this._formatDocResponse ? (swagger ? this._formatDocResponse(swagger) : {}) : swagger,
              };
            });

            responses = { default: { content } };
          }

          this._swaggerJson.paths[fullPath] = _.merge(this._swaggerJson.paths[fullPath], {
            [route.method]: _.merge(route.metadata, {
              tags,
              security,
              parameters,
              requestBody,
              responses,
            }),
          });
        });
      });
    } catch (e) {
      console.log(`[DocumentSwagger.genarateDocument]:`, e);
    }
    return this._swaggerJson;
  }
  //#endregion
}
