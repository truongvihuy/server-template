import _ from 'lodash';
import { FilterQuery, QueryOptions } from 'mongoose';

export type MapFilter<T = any> = (filter?: FilterQuery<T>, projection?: any, options?: QueryOptions | null) => void;

export type MapFilters<T = any> = { [key: string]: MapFilter<T> };

export function processParams<T = any>(
  query: any,
  mapFilters: MapFilters<T>,
  defaultOptions?: QueryOptions,
): [FilterQuery<T>, any, QueryOptions | null] {
  let filter: FilterQuery<T> = {};
  let projection: any = null;
  let options: QueryOptions | null = _.merge({}, defaultOptions ?? {});

  const defaultMapFilters = {
    field: (filter, projection) => {
      projection = query.field.split(',');
    },
    skip: (filter, projection, options) => {
      options.skip = +(query.skip ?? 0);
    },
    limit: (filter, projection, options) => {
      options.limit = +query.limit;
    },
    page: (filter, projection, options) => {
      options.skip = (+query.page - 1) * (+query.limit || defaultOptions?.limit) || 0;
    },
    sort: (filter, projection, options) => {
      const arraySortList = query.sort.split(',');
      options.sort = {};
      for (const e of arraySortList) {
        const [key, order] = e.split(':');
        options = _.merge(options, {
          sort: {
            [key]: 'desc' === order.toLowerCase() ? -1 : 1,
          },
        });
      }
    },
  };

  const mergeMapFilter = _.merge({}, defaultMapFilters, mapFilters);

  Object.keys(query ?? {}).forEach((key) => {
    if (typeof query[key] === 'undefined') {
      return;
    }

    if (mergeMapFilter[key]) {
      mergeMapFilter[key](filter, projection, options);
    } else {
      throw new Error(`query.not.support:${key}`);
    }
  });

  return [filter, projection, options];
}
