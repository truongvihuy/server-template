import Joi from 'joi';

export type AuthConfig = {
  secret: string;
  algorithm: string;
  expiresIn: number | string;
};

export type OptionAuthGuard = { optional?: boolean; joiValidate?: Joi.Schema };
