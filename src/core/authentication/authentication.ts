import { sign, verify } from 'jsonwebtoken';

import { AuthConfig } from './authentication.type';

export default class Authorization<T = any> {
  private _config: AuthConfig;

  constructor(config: AuthConfig) {
    this._config = config;
  }

  public encode(payload: T): string {
    return sign(payload, this._config.secret, {
      algorithm: this._config.algorithm,
      expiresIn: this._config.expiresIn,
    });
  }

  public decode(token: string): T {
    return verify(token, this._config.secret) as T;
  }
}
