import { NextFunction, Request, Response } from 'express-serve-static-core';
import Joi from 'joi';
import 'reflect-metadata';

import { createMiddlewareDecorator, Exception, SecuritiesDefinition, SECURITY_METADATA } from 'core/server';

export const createAuthGuardDecorator =
  <T = any>(decode: (token: string) => T | Promise<T>, metadata: SecuritiesDefinition) =>
  (options: { optional?: boolean; joiValidate: Joi.Schema }) => {
    const authCallback = async (req: Request, _res: Response, next: NextFunction) => {
      const { optional = false, joiValidate = null } = options ?? {};
      const token = req.headers.authorization?.split(' ')?.[1];

      if (!token) {
        if (optional) {
          next();
          return;
        }
        next(Exception.Unauthorized('Unauthorized'));
      }

      const auth = (await decode(token)) as T;
      if (!auth) {
        if (optional) {
          next();
          return;
        }
        throw Exception.Unauthorized('Unauthorized');
      }

      if (joiValidate) {
        const { error } = joiValidate.validate(auth);
        if (error) {
          next(error);
          return;
        }
      }

      next();
    };

    return (target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<any>) => {
      let securities: SecuritiesDefinition[] = [];

      if (Reflect.hasMetadata(SECURITY_METADATA, target.constructor, propertyKey)) {
        securities = Reflect.getMetadata(SECURITY_METADATA, target.constructor, propertyKey);
      }

      securities.push(metadata);
      Reflect.defineMetadata(SECURITY_METADATA, securities, target.constructor, propertyKey as string);

      createMiddlewareDecorator(authCallback)(target, propertyKey, descriptor);
    };
  };
