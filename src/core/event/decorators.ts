import 'reflect-metadata';

import { LISTENER_METADATA, ListenerConfig, ListenerDefinition } from './type';

export const Listener = (eventName: string | symbol, config: ListenerConfig = { type: 'on' }): MethodDecorator => {
  return (target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<any>) => {
    let listeners: ListenerDefinition[] = [];
    if (Reflect.hasMetadata(LISTENER_METADATA, target.constructor)) {
      listeners = Reflect.getMetadata(LISTENER_METADATA, target.constructor);
    }

    listeners.push({ eventName, type: config.type, funcName: propertyKey });
    Reflect.defineMetadata(LISTENER_METADATA, listeners, target.constructor);
  };
};
