import { EventEmitter } from 'events';

import { LISTENER_METADATA, ListenerDefinition } from './type';
export { Listener } from './decorators';

export const applyEventEmitter = (eventEmitter: EventEmitter, eventClass: any, options?: { params: any[] }) => {
  const listeners: ListenerDefinition[] = Reflect.getMetadata(LISTENER_METADATA, eventClass) ?? [];

  const instance = new eventClass(...(options?.params ?? []));
  listeners.forEach((listener) => {
    eventEmitter[listener.type](listener.eventName, instance[listener.funcName].bind(instance));
  });

  return (event: string, ...args: any[]) => {
    eventEmitter.emit(event, ...args);
  };
};

export const checkExistEvent = (eventClass: any): boolean => {
  const listeners: ListenerDefinition[] = Reflect.getMetadata(LISTENER_METADATA, eventClass) ?? [];
  return listeners.length > 0;
};
