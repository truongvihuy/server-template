export type ListenerType = 'on' | 'once';
export interface ListenerConfig {
  type: ListenerType;
}
export interface ListenerDefinition {
  type: ListenerType;
  eventName: string | symbol;
  funcName: string | symbol;
}

export const EVENT_METADATA = 'event';
export const LISTENER_METADATA = 'listener';
