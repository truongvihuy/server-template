import { createServer } from 'http';
import { ExtendedError, Server, ServerOptions, Socket } from 'socket.io';

import GracefulShutDown from 'core/graceful-shutdown';
import MatchMaker, { Stats } from './match-maker';
import { MatchMakerDriver } from './match-maker/driver/type';

type CallbackMiddleware = (socket: Socket, next: (err?: ExtendedError) => void) => void;

export type SocketServerOptionsConfig = {
  port: string | number;
  middleware: CallbackMiddleware[];
  rooms: any[];
  enableHealthChecks: boolean;
  matchMakerDriver: MatchMakerDriver;
  beforeShutdown: () => Promise<void> | void;
} & ServerOptions;

export class SocketServer {
  private port: number | string;
  private rooms: any[];

  constructor(config?: Partial<SocketServerOptionsConfig>) {
    const httpServer = createServer();
    const io = new Server(httpServer, config);
    this.port = config.port || 5000;

    for (const callback of config.middleware ?? []) {
      io.use(callback);
    }

    MatchMaker.setSocket(io);
    MatchMaker.setup(config.matchMakerDriver);

    this.rooms = config.rooms ?? [];

    if (config.beforeShutdown) {
      GracefulShutDown.registerGracefulShutdown(config.beforeShutdown);
    }

    Stats.enableHealthChecks = config.enableHealthChecks;

    this.defineRoom();
  }

  private defineRoom() {
    for (const room of this.rooms) {
      MatchMaker.defineTypeRoom(room);
    }
  }

  public run(callback?: () => void | Promise<void>) {
    MatchMaker.io.listen(+this.port);
    callback();
  }

  public async close() {
    await MatchMaker.io.close();
    await MatchMaker.driver.shutdown();
  }
}
