import { OptionEventRoom } from '../type';

export const NAMESPACE_METADATA = 'namespace';
export const EVENT_ROOM_METADATA = 'event-room';

export enum SocketEventName {
  connected = 'connected',
  message_error = 'message_error',
}

export enum ExceptFuncRoom {
  lock,
  unlock,
  setState,
  onCreate,
  onAuth,
  onJoin,
  onLeave,
  onDispose,
}

/** Namespace */
export type NamespaceDefinition = {
  type: string;
};

/** Event name */
export type EventRoomDefinition = {
  eventName: string;
  funcName: string | symbol;
} & OptionEventRoom;
