import 'reflect-metadata';

import { NAMESPACE_METADATA, NamespaceDefinition } from './constants';

export const Namespace = (type: string): ClassDecorator => {
  // 'target' is Controller
  return (target) => {
    const metadata: NamespaceDefinition = { type };
    Reflect.defineMetadata(NAMESPACE_METADATA, metadata, target);
  };
};
