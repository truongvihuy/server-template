import 'reflect-metadata';

import { OptionEventRoom } from '../type';
import { EVENT_ROOM_METADATA, EventRoomDefinition, ExceptFuncRoom } from './constants';

export const EventRoom = (eventName: string, options: OptionEventRoom): MethodDecorator => {
  return (target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<any>) => {
    if (Object.keys(ExceptFuncRoom).includes(propertyKey.toString())) {
      throw new Error(`Decorator EventRoom not cover '${propertyKey.toString()}' method`);
    }

    let events: EventRoomDefinition[] = [];
    if (Reflect.hasMetadata(EVENT_ROOM_METADATA, target.constructor)) {
      events = Reflect.getMetadata(EVENT_ROOM_METADATA, target.constructor);
    }

    events.push({
      eventName,
      funcName: propertyKey,
      ...options,
    });
    Reflect.defineMetadata(EVENT_ROOM_METADATA, events, target.constructor);
  };
};
