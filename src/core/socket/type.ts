import Joi from 'joi';

export type OptionEventRoom = { optional?: boolean; joiValidate?: Joi.Schema };
