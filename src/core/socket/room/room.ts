import { Socket } from 'socket.io';
import { Handshake } from 'socket.io/dist/socket-types';

import MatchMaker, { getRoomName } from '../match-maker';

export default abstract class Room<Auth = any> {
  private _roomId: string = '';
  protected _maxClient: number = Infinity;
  protected _locked: boolean = false;
  protected _privated: boolean = false;

  constructor() {}

  public get infoRoom() {
    return {
      roomId: this._roomId,
      maxClient: this._maxClient,
      locked: this._locked,
      privated: this._privated,
    };
  }

  public _setRoomId(roomId: string) {
    this._roomId = roomId;
  }
  public _setLocked(locked: boolean) {
    this._locked = locked;
  }
  public _setPrivated(privated: boolean) {
    this._privated = privated;
  }
  public _setMaxClient(_maxClient: number) {
    this._maxClient = _maxClient;
  }

  public lock() {
    MatchMaker.lockRoom(this._roomId);
  }
  public unlock() {
    MatchMaker.unlockRoom(this._roomId);
  }
  public privateRoom(privated: boolean) {
    MatchMaker.setPrivatedRoom(this._roomId, privated);
  }

  public getClientsInRoom() {
    return MatchMaker.getClientsInRoom(this._roomId);
  }

  public getClientsInNsp() {
    const roomName = getRoomName(this._roomId);
    return MatchMaker.getClientsInNsp(roomName);
  }

  public onCreate?(options: Handshake): Promise<void> | void;
  public onAuth?(client: Socket, options: Handshake): Promise<Auth> | Auth;
  public onJoin?(client: Socket, options: Handshake, auth: Auth): Promise<void> | void;
  public onLeave?(client: Socket, consented: boolean): Promise<void> | void;
  public onDispose?(): Promise<void> | void;
}
