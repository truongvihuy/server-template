import { IRoomData, MatchMakerDriver } from '../type';
import RoomCache from './room-data';

export default class RedisDriver implements MatchMakerDriver {
  constructor(connectionString: string) {}

  public async createInstance(initialValues: Partial<IRoomData> = {}) {
    const roomData = new RoomCache(initialValues, null);
    await roomData.save();
    return roomData;
  }

  public has(roomId: string) {
    return true;
  }

  public find(conditions: Partial<IRoomData>) {
    return [new RoomCache(conditions, null)];
  }

  public async cleanup(processId: number) {}

  public async findOne(conditions: Partial<IRoomData>) {
    return new RoomCache(conditions, null);
  }

  public async clear() {}

  public async shutdown() {}
}
