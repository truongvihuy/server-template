import { RoomData } from '../type';

export default class RoomCache implements RoomData {
  public name: string;
  public maxClients: number = Infinity;

  public roomId: string;
  public clients: number = 0;
  public locked: boolean = false;
  public privated: boolean = false;
  public processId: number = process.pid;

  constructor(initialValues: any, model: any) {
    for (const field in initialValues) {
      if (initialValues.hasOwnProperty(field)) {
        this[field] = initialValues[field];
      }
    }
  }

  public async save() {}

  public async updateOne(operations: any) {}

  public async remove() {}
}
