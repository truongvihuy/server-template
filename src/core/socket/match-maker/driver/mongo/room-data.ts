import { Model, Schema } from 'mongoose';
import { IRoomData, RoomData } from '../type';

export const RoomSchema = new Schema<IRoomData, Model<IRoomData>>(
  {
    name: { type: String, required: true },
    maxClients: { type: Number, required: true, default: Infinity },
    clients: { type: Number, default: 0 },
    roomId: { type: String, index: true },
    locked: { type: Boolean, index: true },
    privated: { type: Boolean, index: true },
    processId: { type: Number, index: true },
  },
  {
    versionKey: false,
  },
);

export default class RoomCache implements RoomData {
  public name: string;
  public maxClients: number = Infinity;

  public roomId: string;
  public clients: number = 0;
  public locked: boolean = false;
  public privated: boolean = false;
  public processId: number = process.pid;

  private model: Model<IRoomData>;

  constructor(initialValues: any, model: Model<IRoomData>) {
    for (const field in initialValues) {
      if (initialValues.hasOwnProperty(field)) {
        this[field] = initialValues[field];
      }
    }

    this.model = model;
  }

  public async save() {
    await this.model.updateOne(
      { processId: this.processId, roomId: this.roomId },
      {
        $set: {
          name: this.name,
          maxClients: this.maxClients,

          roomId: this.roomId,
          clients: this.clients,
          locked: this.locked,
          privated: this.privated,
          processId: this.processId,
        },
      },
      { upsert: true },
    );
  }

  public async updateOne(operations: any) {
    if (operations.$set) {
      for (const field in operations.$set) {
        if (operations.$set.hasOwnProperty(field)) {
          this[field] = operations.$set[field];
        }
      }
    }

    if (operations.$inc) {
      for (const field in operations.$inc) {
        if (operations.$inc.hasOwnProperty(field)) {
          this[field] += operations.$inc[field];
        }
      }
    }

    await this.save();
  }

  public async remove() {
    await this.model.deleteOne({ processId: this.processId, roomId: this.roomId });
  }
}
