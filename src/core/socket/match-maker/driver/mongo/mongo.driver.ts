import { Model } from 'mongoose';

import { MongodbService } from 'core/mongodb';
import { IRoomData, MatchMakerDriver, RoomData } from '../type';
import RoomCache, { RoomSchema } from './room-data';

export default class MongoDriver implements MatchMakerDriver {
  private model: Model<IRoomData>;
  private db: MongodbService;
  constructor(connectionString: string) {
    this.db = new MongodbService({ connectionString });
    const conn = this.db.connect();

    this.model = conn?.model<IRoomData>('room', RoomSchema);
  }

  public async createInstance(initialValues: Partial<IRoomData> = {}) {
    const roomData = new RoomCache(initialValues, this.model);
    await roomData.save();
    return roomData;
  }

  public has(roomId: string) {
    return !!this.model.exists({ roomId });
  }

  public find(conditions: Partial<IRoomData>) {
    return this.model.find(conditions, null, { lean: true }).then((dataList) => {
      return dataList.map((data) => new RoomCache(data, this.model));
    });
  }

  public async cleanup(processId: number) {
    await this.model.deleteMany({ processId });
  }

  public async findOne(conditions: Partial<IRoomData>) {
    return this.model
      .findOne(conditions, null, { lean: true })
      .then((data) => (data ? new RoomCache(data, this.model) : null));
  }

  public async clear() {
    await this.model.deleteMany({});
  }

  public async shutdown() {
    // await this.db.close();
  }
}
