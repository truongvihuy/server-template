import { IRoomData, RoomData } from '../type';

export default class RoomCache implements IRoomData {
  public name: string;
  public maxClients: number = Infinity;

  public roomId: string;
  public clients: number = 0;
  public locked: boolean = false;
  public privated: boolean = false;
  public processId: number = process.pid;

  private $rooms: RoomCache[];

  constructor(initialValues: any, rooms: RoomData[]) {
    for (const field in initialValues) {
      if (initialValues.hasOwnProperty(field)) {
        this[field] = initialValues[field];
      }
    }

    Object.defineProperty(this, '$rooms', {
      value: rooms,
      enumerable: false,
      writable: true,
    });
  }

  public save() {
    if (this.$rooms.indexOf(this) === -1) {
      this.$rooms.push(this);
    }
  }

  public updateOne(operations: any) {
    if (operations.$set) {
      for (const field in operations.$set) {
        if (operations.$set.hasOwnProperty(field)) {
          this[field] = operations.$set[field];
        }
      }
    }

    if (operations.$inc) {
      for (const field in operations.$inc) {
        if (operations.$inc.hasOwnProperty(field)) {
          this[field] += operations.$inc[field];
        }
      }
    }
  }

  public remove() {
    //
    // WORKAROUND: prevent calling `.remove()` multiple times
    // Seems to happen during disconnect + dispose: https://github.com/colyseus/colyseus/issues/390
    //
    if (!this.$rooms) {
      return;
    }

    const roomIndex = this.$rooms.indexOf(this);
    if (roomIndex === -1) {
      return;
    }

    const len = this.$rooms.length - 1;
    for (let i = roomIndex; i < len; i++) {
      this.$rooms[i] = this.$rooms[i + 1];
    }

    this.$rooms.length = len;
  }
}
