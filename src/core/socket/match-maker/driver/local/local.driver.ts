import { IRoomData, MatchMakerDriver } from '../type';
import RoomCache from './room-data';

export default class LocalDriver implements MatchMakerDriver {
  public rooms: RoomCache[] = [];

  public createInstance(initialValues: Partial<IRoomData> = {}) {
    if (!this.rooms) {
      this.rooms = [];
    }

    const roomData = new RoomCache(initialValues, this.rooms);
    roomData.save();

    return roomData;
  }

  public has(roomId: string) {
    return this.rooms.some((room) => room.roomId === roomId);
  }

  public find(conditions: Partial<IRoomData>) {
    return this.rooms.filter((room) => {
      for (const field in conditions) {
        if (conditions.hasOwnProperty(field) && room[field] !== conditions[field]) {
          return false;
        }
      }

      return true;
    });
  }

  public cleanup(processId: number) {
    const cachedRooms = this.find({ processId });
    cachedRooms.forEach((room) => room.remove());
  }

  public findOne(conditions: Partial<IRoomData>) {
    const result = this.rooms.find((room) => {
      for (const field in conditions) {
        if (conditions.hasOwnProperty(field) && room[field] !== conditions[field]) {
          return false;
        }
      }
      return true;
    });

    return result;
  }

  public clear() {
    this.rooms = [];
  }

  public shutdown() {}
}
