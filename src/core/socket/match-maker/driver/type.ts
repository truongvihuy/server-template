export interface IRoomData {
  name: string;
  maxClients: number;

  roomId: string;
  clients: number;
  locked: boolean;
  privated: boolean;
  processId: number;
}

export interface RoomData extends IRoomData {
  updateOne(operations: { $set?: Partial<IRoomData>; $inc?: Partial<IRoomData> });
  save();
  remove();
}

export interface MatchMakerDriver {
  createInstance(initialValues: Partial<IRoomData>): Promise<RoomData> | RoomData;
  has(roomId: string): Promise<boolean> | boolean;
  find(conditions: Partial<IRoomData>): Promise<RoomData[]> | RoomData[];
  cleanup(processId: number): Promise<void> | void;
  findOne(conditions: Partial<IRoomData>): Promise<RoomData | null> | RoomData | null;
  clear(): Promise<void> | void;
  shutdown(): Promise<void> | void;
}
