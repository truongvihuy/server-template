export const generateIdRoom = (roomName: string) => {
  return `${roomName}:${+new Date()}`;
};

export const verifyIdRoom = (roomId: string, roomName: string) => {
  return roomId.startsWith(`${roomName}:`);
};

export const getRoomName = (roomId: string) => {
  return roomId.split(':')[0];
};
