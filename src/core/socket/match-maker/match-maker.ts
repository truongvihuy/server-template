import { Namespace, Server, Socket } from 'socket.io';

import { applyEventEmitter } from 'core/event';
import { NAMESPACE_METADATA, NamespaceDefinition } from '../decorators';
import AdapterEmitter, { EVENT_ADAPTER_EMTTIER } from '../emitter/adapter.emitter';
import NamespaceEmitter from '../emitter/namespace.emitter';
import { LocalDriver } from './driver/local';
import { IRoomData, MatchMakerDriver, RoomData } from './driver/type';
import { generateIdRoom, getRoomName } from './utils';

export default class MatchMaker {
  public static io: Server;
  private static emitterAdapter: Map<string, any>;

  public static driver: MatchMakerDriver;

  public static setSocket(socket: Server) {
    this.io = socket;
  }

  public static setup(driver?: MatchMakerDriver) {
    this.driver = driver ?? new LocalDriver();
    this.emitterAdapter = new Map();
  }

  public static async join(socket: Socket, roomName: string) {
    const room = await this.findOneRoomAvailable(roomName);

    if (!room) {
      throw new Error(`Room "${roomName}" not found`);
    }

    await this.handleJoinRoom(socket, room);
  }

  public static async create(socket: Socket, roomName: string) {
    const room = await this.createRoom(roomName);
    await this.handleJoinRoom(socket, room);
  }

  public static async joinOrCreate(socket: Socket, roomName: string) {
    let room = await this.findOneRoomAvailable(roomName);

    if (!room) {
      room = await this.createRoom(roomName);
    }

    await this.handleJoinRoom(socket, room);
  }

  public static async joinById(socket: Socket, roomId: string) {
    const room = await this.driver.findOne({ roomId });

    if (!room) {
      throw new Error(`RoomId "${roomId}" not found`);
    }
    if (room.locked) {
      throw new Error(`RoomId "${roomId}" is locked`);
    }

    await this.handleJoinRoom(socket, room);
  }

  public static query(conditions: Partial<IRoomData> = {}) {
    return this.driver.find(conditions);
  }

  public static async findOneRoomAvailable(roomName: string) {
    return this.driver.findOne({
      locked: false,
      privated: false,
      name: roomName,
    });
  }

  private static async handleJoinRoom(socket: Socket, roomData: RoomData) {
    const emit = this.emitterAdapter.get(roomData.name);
    emit(EVENT_ADAPTER_EMTTIER.verifyAuth, roomData.roomId, socket.id);
  }

  public static async createRoom(roomName: string) {
    const roomId = generateIdRoom(roomName);
    const room = await this.driver.createInstance({ name: roomName, roomId });
    return room;
  }

  public static async lockRoom(roomId: string) {
    const room = await this.driver.findOne({ roomId });
    if (!room) {
      throw new Error(`RoomId "${roomId}" not found`);
    }
    if (room.locked !== true) {
      const emit = this.emitterAdapter.get(room.name);
      emit(EVENT_ADAPTER_EMTTIER.lockRoom, roomId, true);
    }
  }

  public static async unlockRoom(roomId: string) {
    const room = await this.driver.findOne({ roomId });
    if (!room) {
      throw new Error(`RoomId "${roomId}" not found`);
    }

    if (room.locked !== false) {
      const emit = this.emitterAdapter.get(room.name);
      emit(EVENT_ADAPTER_EMTTIER.lockRoom, roomId, false);
    }
  }

  public static async setPrivatedRoom(roomId: string, privated: boolean) {
    const room = await this.driver.findOne({ roomId });
    if (!room) {
      throw new Error(`RoomId "${roomId}" not found`);
    }

    if (room.privated !== privated) {
      const emit = this.emitterAdapter.get(room.name);
      emit(EVENT_ADAPTER_EMTTIER.privatedRoom, roomId, privated);
    }
  }

  public static defineTypeRoom(ctrRoom: any) {
    const metadataRoom: NamespaceDefinition = Reflect.getMetadata(NAMESPACE_METADATA, ctrRoom);
    const nsp = this.io.of(`/${metadataRoom.type}`);

    this.emitterAdapter.set(
      metadataRoom.type,
      applyEventEmitter(nsp.adapter, AdapterEmitter, { params: [MatchMaker, ctrRoom, metadataRoom] }),
    );
    applyEventEmitter(nsp, NamespaceEmitter);
  }

  public static getClientsInRoom(roomId: string) {
    const roomName = getRoomName(roomId);
    const nsp = this.io.of(`/${roomName}`);

    return this._filterClients(nsp, (socket) => socket.rooms.has(roomId));
  }

  public static getClientsInNsp(roomName: string) {
    const nsp = this.io.of(`/${roomName}`);
    return this._filterClients(nsp, () => true);
  }

  private static _filterClients(nsp: Namespace, conditionCallBack: (socket: Socket) => boolean) {
    const clients: Socket[] = [];
    nsp.sockets.forEach((socket) => {
      if (conditionCallBack(socket)) {
        clients.push(socket);
      }
    });

    return clients;
  }
}
