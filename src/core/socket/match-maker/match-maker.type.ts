import { RoomData } from './driver/type';

export const INVALID_OPTION_KEYS: Array<keyof RoomData> = [
  'clients',
  'locked',
  'privated',
  // 'maxClients', - maxClients can be useful as filter options
  'name',
  'processId',
  'roomId',
];

export { RoomData };
