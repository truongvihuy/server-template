type Stats = {
  processId: number;
  ccu: number;
  roomCount: number;
};

export default class StatsProcess {
  public static local: Stats = {
    processId: process.pid,
    ccu: 0,
    roomCount: 0,
  };
  public static enableHealthChecks: boolean = true;
  public static handleWrite: (stats: Stats) => Promise<void>;
  public static handleRead: () => Promise<Stats[]>;

  public static incCCU(num: number) {
    this.local.ccu = (this.local.ccu ?? 0) + num;
    this.persist();
  }
  public static incRoom(num: number) {
    this.local.roomCount = (this.local.roomCount ?? 0) + num;
    this.persist();
  }

  private static readonly persistInterval = 1000;
  private static lastPersisted = 0;
  private static persistTimeout = undefined;
  public static persist(forceNow: boolean = false) {
    if (!this.enableHealthChecks) {
      clearTimeout(this.persistTimeout);
      return;
    }
    const now = Date.now();

    if (forceNow || now - this.lastPersisted > this.persistInterval) {
      this.lastPersisted = now;

      return this.handleWrite(this.local);
    } else {
      clearTimeout(this.persistTimeout);
      this.persistTimeout = setTimeout(this.persist, this.persistInterval);
    }
  }
  public static reset(_persist: boolean = true) {
    this.local.roomCount = 0;
    this.local.ccu = 0;

    if (_persist) {
      this.lastPersisted = 0;
      clearTimeout(this.persistTimeout);
      this.persist();
    }
  }

  public static async fetchAll() {
    return this.handleRead();
  }

  public static async getGlobalCCU() {
    const allStats = await this.handleRead();
    return allStats.reduce((prev, next) => prev + next.ccu, 0);
  }
}
