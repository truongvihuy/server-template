import { DisconnectReason, Socket } from 'socket.io';

import { Listener } from 'core/event';
import { Room } from '../room';

export default class SocketEmitter {
  constructor(private socket: Socket, private room: Room) {}
  // @Listener('connect', { type: 'on' })
  // public connect(...args) {}

  // @Listener('connect_error', { type: 'on' })
  // public connectError(...args) {}

  @Listener('disconnecting', { type: 'on' })
  public async disconnecting(reason: DisconnectReason) {
    let consented = true;
    switch (reason) {
      case 'server namespace disconnect':
      case 'client namespace disconnect': {
        consented = true;
        break;
      }

      case 'parse error':
      case 'forced close':
      case 'forced server close':

      case 'server shutting down':
      case 'ping timeout':
      case 'transport error':
      case 'transport close': {
        consented = false;
        break;
      }
    }
    this.room.onLeave && (await this.room.onLeave(this.socket, consented));
  }

  @Listener('disconnect', { type: 'on' })
  public disconnect(reason: DisconnectReason) {}

  // @Listener('newListener', { type: 'on' })
  // public newListener(eventName: string, listenner: (...args: any[]) => void) {}

  // @Listener('removeListener', { type: 'on' })
  // public removeListener(eventName: string, listenner: (...args: any[]) => void) {}
}
