import { Namespace } from 'socket.io';

import { applyEventEmitter, Listener } from 'core/event';
import Joi from 'core/joi';
import { EVENT_ROOM_METADATA, EventRoomDefinition, NamespaceDefinition, SocketEventName } from '../decorators';
import { RoomData, Stats, verifyIdRoom } from '../match-maker';
import { Room } from '../room';
import SocketEmitter from './socket.emitter';

export enum EVENT_ADAPTER_EMTTIER {
  createRoom = 'create-room',
  verifyAuth = 'verify-auth',
  joinRoom = 'join-room',
  leaveRoom = 'leave-room',
  deleteRoom = 'delete-room',
  lockRoom = 'lock-room',
  privatedRoom = 'private-room',
}

export default class AdapterEmitter {
  private nsp: Namespace;
  private rooms: Map<string, Room>;
  private listeners: EventRoomDefinition[];

  constructor(private matchMaker: any, private ctrRoom: any, private metadata: NamespaceDefinition) {
    this.nsp = this.matchMaker.io.of(`/${metadata?.type ?? ''}`) as Namespace;
    this.listeners = (Reflect.getMetadata(EVENT_ROOM_METADATA, ctrRoom) ?? []) as EventRoomDefinition[];
    this.rooms = new Map<string, Room>();
  }

  @Listener(EVENT_ADAPTER_EMTTIER.createRoom, { type: 'on' })
  public async createRoom(roomId: string) {
    if (verifyIdRoom(roomId, this.metadata.type)) {
      // let room = this.rooms.get(roomId);
      // if (!room) {
      //   room = new this.ctrRoom() as Room;
      //   room.setRoomId(roomId);
      //   this.rooms.set(roomId, room);
      // }
    }
  }

  @Listener(EVENT_ADAPTER_EMTTIER.verifyAuth, { type: 'on' })
  public async verify(roomId: string, socketId: string) {
    if (verifyIdRoom(roomId, this.metadata.type)) {
      const socket = this.nsp.sockets.get(socketId);
      let room = this.rooms.get(roomId);
      if (!room) {
        Stats.incRoom(1);

        room = new this.ctrRoom() as Room;
        room._setRoomId(roomId);
        this.rooms.set(roomId, room);

        const roomData = (await this.matchMaker.driver.findOne({ roomId })) as RoomData;
        const locked = roomData.clients + 1 >= room.infoRoom.maxClient;
        await roomData.updateOne({
          $set: {
            maxClients: room.infoRoom.maxClient,
            locked: room.infoRoom.locked || locked,
            privated: room.infoRoom.privated,
          },
        });
        room.onCreate && (await room.onCreate(socket.handshake));
      } else {
        const roomData = (await this.matchMaker.driver.findOne({ roomId })) as RoomData;
        if (!roomData.locked) {
          const locked = roomData.clients + 1 >= roomData.maxClients;
          if (locked) {
            this.lockRoom(roomId, true);
          }
        }
      }

      applyEventEmitter(socket, SocketEmitter, { params: [socket, room] });
      const auth = room.onAuth && (await room.onAuth(socket, socket.handshake));
      socket.join(roomId);
      room.onJoin && (await room.onJoin(socket, socket.handshake, auth));
      socket.emit(SocketEventName.connected, auth);
    }
  }

  @Listener(EVENT_ADAPTER_EMTTIER.joinRoom, { type: 'on' })
  public async joinRoom(roomId: string, socketId: string) {
    if (verifyIdRoom(roomId, this.metadata.type)) {
      Stats.incCCU(1);
      this.applyEvent(roomId, socketId);

      const roomData = (await this.matchMaker.driver.findOne({ roomId })) as RoomData;
      await roomData.updateOne({ $inc: { clients: 1 } });
    } else {
      const socket = this.nsp.sockets.get(socketId);
      const funcName = socket.handshake.auth.action;
      const roomNameOrRoomId = socket.handshake.auth.roomId ?? this.metadata.type;

      try {
        await this.matchMaker[funcName](socket, roomNameOrRoomId);
      } catch (error: any) {
        socket.emit(SocketEventName.message_error, {
          message: `${error.message}`,
          eventName: funcName,
          stack: `${error.stack}`,
        });
        socket.disconnect();
      }
    }
  }

  @Listener(EVENT_ADAPTER_EMTTIER.leaveRoom, { type: 'on' })
  public async leaveRoom(roomId: string, socketId: string) {
    if (verifyIdRoom(roomId, this.metadata.type)) {
      Stats.incCCU(-1);

      const socket = this.nsp.sockets.get(socketId);
      this.listeners.forEach((ls) => {
        socket.removeAllListeners(ls.eventName);
      });

      const roomData = (await this.matchMaker.driver.findOne({ roomId })) as RoomData;
      await roomData.updateOne({ $inc: { clients: -1 } });
    }
  }

  @Listener(EVENT_ADAPTER_EMTTIER.deleteRoom, { type: 'on' })
  public async deleteRoom(roomId) {
    if (verifyIdRoom(roomId, this.metadata.type)) {
      Stats.incRoom(-1);

      const room = this.rooms.get(roomId);
      room.onDispose && (await room.onDispose());

      this.rooms.delete(roomId);
      const roomData = (await this.matchMaker.driver.findOne({ roomId })) as RoomData;
      await roomData.remove();
    }
  }

  @Listener(EVENT_ADAPTER_EMTTIER.lockRoom, { type: 'on' })
  public async lockRoom(roomId: string, locked: boolean) {
    if (verifyIdRoom(roomId, this.metadata.type)) {
      const room = this.rooms.get(roomId);
      room._setLocked(locked);
      const roomData = (await this.matchMaker.driver.findOne({ roomId })) as RoomData;
      await roomData.updateOne({ $set: { locked } });
    }
  }

  @Listener(EVENT_ADAPTER_EMTTIER.privatedRoom, { type: 'on' })
  public async privatedRoom(roomId: string, privated: boolean) {
    if (verifyIdRoom(roomId, this.metadata.type)) {
      const room = this.rooms.get(roomId);
      room._setPrivated(privated);
      const roomData = (await this.matchMaker.driver.findOne({ roomId })) as RoomData;
      await roomData.updateOne({ $set: { privated } });
    }
  }

  private async applyEvent(roomId: string, socketId: string) {
    const socket = this.nsp.sockets.get(socketId);
    const room = this.rooms.get(roomId);

    this.listeners.forEach((listener) => {
      const { optional = false, joiValidate = null, funcName, eventName } = listener ?? {};

      socket.on(eventName, async (param) => {
        let _param = param;
        if ([undefined, null].includes(param) && !optional) {
          const { error } = Joi.required().validate(undefined);
          socket.emit(SocketEventName.message_error, {
            message: `${error.message}`,
            eventName,
            stack: `${error.stack}`,
          });

          return;
        }

        if (joiValidate) {
          const { value, error } = joiValidate.validate(param);
          if (error) {
            socket.emit(SocketEventName.message_error, {
              message: `${error.message}`,
              eventName,
              stack: `${error.stack}`,
            });

            return;
          }

          _param = value;
        }

        try {
          await room[funcName](socket, _param);
        } catch (error: any) {
          socket.emit(SocketEventName.message_error, {
            message: `${error.message}`,
            eventName,
            stack: `${error.stack}`,
          });
        }
      });
    });
  }
}
