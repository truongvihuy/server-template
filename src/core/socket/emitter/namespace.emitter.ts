import { Socket } from 'socket.io';

import { Listener } from 'core/event';

export default class NamespaceEmitter {
  @Listener('connection', { type: 'on' })
  public async connection(socket: Socket) {}
}
