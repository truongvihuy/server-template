class TimeoutError extends Error {
  override name = 'TimeoutError';
}

/**
 * Detect if error is a Timeout error.
 */
export function isTimeoutError(err: Error): boolean {
  return err instanceof TimeoutError;
}

export class PromiseWithTimer {
  private timerId: NodeJS.Timeout | undefined;
  private static timeoutSymbol = Symbol('TimeoutSymbol');

  public sleep(ms: number): Promise<symbol> {
    return new Promise((resolve) => setTimeout(resolve, ms, PromiseWithTimer.timeoutSymbol));
  }

  public async startTimer<T>(promise: Promise<T>): Promise<{ executionTime: number; result: T }> {
    const startTime = Date.now();
    const result: T = await promise;
    const endTime = Date.now();
    const executionTime: number = endTime - startTime;
    return { executionTime, result };
  }

  public async startRace<T>(promise: Promise<T>, ms?: number): Promise<T> {
    if (!ms || ms <= 0) {
      return promise;
    }

    const result = await Promise.race([promise, this.sleep(ms)]);

    if (result === PromiseWithTimer.timeoutSymbol) {
      throw new TimeoutError('timeout');
    }

    clearTimeout(this.timerId);
    return result as T;
  }
}
