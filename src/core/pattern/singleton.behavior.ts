export default class Singleton {
  public static instance: { [key: string]: any } = {};

  public static get(key: string): any {
    return this.instance[key];
  }

  public static set(key: string, value: any): void {
    this.instance[key] = value;
  }

  public static clearAll(): void {
    this.instance = {};
  }
}
