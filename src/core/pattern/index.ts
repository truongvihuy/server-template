export { default as SequentialProcessing, createClassSequentialProcesssing } from './sequential-processing.behavior';
export { default as Singleton } from './singleton.behavior';
