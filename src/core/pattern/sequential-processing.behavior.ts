import EventEmitter from 'events';
import fs from 'fs';

type Options = {
  path?: string;
  reprocess?: boolean; // auto push in queue if fail
};

enum EVENT {
  push = 'push',
  preHandle = 'pre-handle',
  postHandle = 'pos-handle',
}

export default abstract class SequentialProcessing<T = any, R = any> {
  private idCount = 0;
  private _pools: { input: T; hash?: string }[] = [];
  private idle: boolean = true;
  private locked: boolean = false;
  private event: EventEmitter;
  private options: Options = {};

  constructor(options: Options = {}) {
    this.options = options;
    this.event = new EventEmitter();

    if (this.options.path && fs.existsSync(this.options.path)) {
      const txt = fs.readFileSync(this.options.path, 'utf-8');
      this._pools = txt ? (JSON.parse(txt) as T[]).map((input) => ({ input })) : [];
      if (this._pools.length) {
        setTimeout(() => {
          this.nextHandle();
        }, 1000);
      }
    }
  }

  get pools() {
    return this._pools.map((e) => e.input);
  }

  get status(): number {
    return this._pools.length;
  }

  public lock() {
    this.locked = true;
  }

  public unlock() {
    this.locked = false;
    this.nextHandle();
  }

  abstract handleProcessing(value: T): Promise<R>;

  on(eventName: EVENT, cb: (...args) => void) {
    this.event.on(eventName, cb);
  }

  once(eventName: EVENT, cb: (...args) => void) {
    this.event.once(eventName, cb);
  }

  private nextHandle(): void {
    if (this.idle && !this.locked) {
      this.options.path && fs.writeFileSync(this.options.path, JSON.stringify(this._pools.map((_) => _.input)));
      if (this._pools.length) {
        this.idle = false;
        const { hash, input } = this._pools.shift();
        this.event.emit(EVENT.preHandle, hash, input);
        this.handleProcessing(input)
          .then((result) => {
            hash && this.event.emit(`${hash}:success`, result);
            this.event.emit(EVENT.preHandle, hash, input, result, null);

            this.idle = true;
            this.nextHandle();
          })
          .catch((e) => {
            hash && this.event.emit(`${hash}:error`, e);
            this.event.emit(EVENT.preHandle, hash, input, null, e);

            this.options.reprocess && this.pushToPool(input);

            this.idle = true;
            this.nextHandle();
          });
      }
    }
  }

  public pushToPool(...args: T[]): string[] {
    const hashList: string[] = [];
    for (const input of args) {
      const hash = `${this.idCount++}`;
      this.event.emit(EVENT.push, hash, input);
      this._pools.push({ hash, input });
      hashList.push(hash);
    }

    this.nextHandle();
    return hashList;
  }

  public waitProcess(input: T): Promise<R> {
    const [hash] = this.pushToPool(input);
    return this.listenProcess(hash);
  }

  public listenProcess(hash: string): Promise<R> {
    return new Promise<R>((resolve, reject) => {
      this.event.once(`${hash}:success`, resolve);
      this.event.once(`${hash}:error`, reject);
    });
  }
}

export const createClassSequentialProcesssing = <T = any, R = any>(callback: (v: T) => Promise<R>) => {
  return class cusSequentialProcesssing extends SequentialProcessing<T, R> {
    public handleProcessing(value: T): Promise<R> {
      return callback(value);
    }
  };
};
