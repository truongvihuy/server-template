import { DbConfig } from 'config/mongodb.config';
import { MongodbService } from 'core/mongodb';

export const DbService = new MongodbService(DbConfig);
