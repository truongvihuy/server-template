import winston from 'winston';

import LoggerConfig from 'config/logger.config';

const logger = winston.createLogger(LoggerConfig);

export default logger;
