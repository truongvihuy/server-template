import { handleErrorRoutes, handleResponse, middlewareApp, middlewareRoutes } from 'config/middleware';
import controllers from 'config/routes.config';
import ServerConfig from 'config/server.config';
import { Server } from 'core/server';

const app = new Server({
  ...ServerConfig.config,
  middleware: middlewareApp,
});

app.applyMiddleware({
  controllers,
  bodyParserConfig: true,
  cors: true,
  middleware: middlewareRoutes,
  handleError: handleErrorRoutes,
  handleResponse: handleResponse,
});

export default app;
