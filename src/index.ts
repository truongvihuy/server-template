import 'colors';

import ServerConfig from 'config/server.config';
import server from 'services/app';
import logger from 'services/logger';

const main = () => {
  server.run(() => {
    logger.info(
      `🚀🚀🚀 Started http://localhost${ServerConfig.config.port != 80 ? `:${ServerConfig.config.port}` : ''}${
        ServerConfig.config.path
      } 🚀🚀🚀`,
    );
  });
};

main();
