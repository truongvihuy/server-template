import _ from 'lodash';

import ServerConfig from './server.config';

const DbConfig = _.merge(ServerConfig.db, {
  emitter: null,
});

export { DbConfig };
