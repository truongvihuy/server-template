import { NextFunction, Request, Response } from 'express';

import logger from 'services/logger';
import ServerConfig from './server.config';

const startTimer = (req: any, _: Response, next: NextFunction) => {
  req.startTimer = Date.now();
  next();
};

const stopTimer = (req: any) => {
  const startTimer = req.startTimer;
  const stopTimer = Date.now();
  return stopTimer - startTimer;
};

const logRequest = (req: Request, _: Response, next: NextFunction) => {
  let message = `${req.method.yellow} ${decodeURI(req.path)}`;
  if (ServerConfig.env !== 'test') {
    if (req.query && Object.keys(req.query).length) {
      message += `\n${'query'.yellow} ${JSON.stringify(req.query)}`;
    }
    if (req.body && Object.keys(req.body).length) {
      message += `\n${'payload'.yellow} ${JSON.stringify(req.body)}`;
    }
  }
  logger.info(message);
  next();
};

const logError = (error: Error, _: Request, __: Response, next: NextFunction) => {
  logger.error(error);
  next(error);
};

const handleErrorResponse = (error: Error, _: Request, res: Response, __: NextFunction) => {
  const response: any = {
    error: {
      message: error.message,
    },
    success: false,
  };
  if (error.stack && !['production'].includes(ServerConfig.env)) {
    response.error.stack = error.stack;
  }
  res.status(500).send(response);
};

export const middlewareApp = [startTimer];

export const middlewareRoutes = [logRequest];

export const handleResponse = (data: any, error: any, req: Request, res: Response, next: NextFunction) => {
  if (error) {
    const executionTime = stopTimer(req);
    logger.info(`${'Fail'.red}, ${`${executionTime}ms`.yellow}`);
    next(error);
  } else {
    const executionTime = stopTimer(req);
    logger.info(`${'Success'.green}, ${`${executionTime}ms`.yellow}`);
    if ([true, false, null, undefined].includes(data)) {
      res.send({
        execution_time: executionTime,
        success: data ?? true,
      });
    } else {
      res.send({
        execution_time: executionTime,
        data: data,
        success: true,
      });
    }
  }
};

export const handleErrorRoutes = [logError, handleErrorResponse];
