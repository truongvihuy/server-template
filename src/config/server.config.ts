import fs from 'fs';

const pkJson: any = (() => {
  return JSON.parse(fs.readFileSync('./package.json', 'utf-8'));
})();

const ServerConfig = {
  name: pkJson.name,
  description: pkJson.description,
  author: pkJson.author,
  license: pkJson.license,
  version: pkJson.version,
  env: process.env.NODE_ENV || 'development',
  secret: process.env.SECRET || 'diaries@123',
  timeZone: process.env.TZ,

  info: {
    name: pkJson.name,
    description: pkJson.description,
    author: pkJson.author,
    license: pkJson.license,
    version: pkJson.version,
    env: process.env.NODE_ENV || 'development',
  },
  config: {
    path: process.env.API_ENDPOINT || '/api',
    port: process.env.PORT || 4000,
    pid: process.pid,
    document: ['true', 'false'].includes(process.env.DOCUMENT?.toLowerCase() as string)
      ? Boolean(process.env.DOCUMENT?.toLowerCase() as string)
      : process.env.DOCUMENT ?? false,
  },
  db: {
    connectionString: process.env.DB_CONNECTION_STRING,
    // host: process.env.DB_HOST || 'localhost',
    // port: process.env.DB_PORT || 27017,
    // dbName: process.env.DB_NAME || 'diary',
    // username: process.env.DB_USERNAME,
    // password: process.env.DB_PASSWORD,
  },
  // admin: {
  //   username: (process.env.USERNAME || 'admin').toLowerCase(),
  //   password: process.env.PASSWORD || 'admin',
  // },
};

export default ServerConfig;
