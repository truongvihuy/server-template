import 'colors'; // import in ENV test
import { FOLDER_LOG } from 'utils/constants';
import winston from 'winston';
import ServerConfig from './server.config';

const LoggerConfig: winston.LoggerOptions = {
  format: winston.format.combine(
    winston.format((info) => {
      if (info instanceof Error) {
        return Object.assign({}, info, {
          stack: info.stack,
          message: info.message,
        });
      }
      return info;
    })(),
    winston.format.timestamp({
      format: () => {
        return new Date().toLocaleString('en-GB', {
          timeZone: 'Asia/Bangkok',
        });
      },
    }),
  ),
  transports: [],
};
if (ServerConfig.env !== 'test') {
  (LoggerConfig.transports as winston.transport[]).push(
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.printf((info) => {
          return `[${info.timestamp}] (${ServerConfig.env.yellow}) [${info.level}]: ${info.stack ?? info.message}`;
        }),
      ),
    }),
  );
}

if (FOLDER_LOG) {
  (LoggerConfig.transports as winston.transport[]).push(
    new winston.transports.File({
      dirname: FOLDER_LOG,
      filename: ServerConfig.env ? `${ServerConfig.env}.error.log` : 'error.log',
      level: 'error',
      format: winston.format.printf((info) => {
        return `[${info.timestamp}] [${info.level}]: ${info.stack ?? info.message}`;
      }),
    }),
  );
}

export default LoggerConfig;
