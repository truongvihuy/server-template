import ServerConfig from 'config/server.config';
import { Controller, Get } from 'core/server';

@Controller()
class AppController {
  @Get()
  public infoApp() {
    return ServerConfig.info;
  }
}

export default AppController;
