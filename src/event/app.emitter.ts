import { applyEventEmitter, Listener } from 'core/event';
import { EventEmitter } from 'events';
import { Log } from 'utils/decorate';

class AppEmitter {
  @Log
  @Listener('app-event')
  public async handleEvent(...args: any[]) {
    console.log(args);
  }
}

const appEvent = new EventEmitter();
applyEventEmitter(appEvent, AppEmitter);

export const emitAppEvent = (...args: any[]) => {
  appEvent.emit('app-event', ...args);
};
