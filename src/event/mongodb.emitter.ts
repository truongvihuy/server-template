import { Listener } from 'core/event';
import logger from 'services/logger';
import { DbService } from 'services/mongodb';

export class DbEmitter {
  @Listener('open', { type: 'once' })
  public open() {
    const con = DbService.connect();
    logger.info(`[${con.name.yellow}] ✔️✔️✔️ Mongodb connected ✔️✔️✔️`);
  }

  @Listener('error', { type: 'on' })
  public error() {
    const con = DbService.connect();
    logger.error(`[${con.name.yellow}] ⚠️⚠️⚠️ Connection error ⚠️⚠️⚠️`);
    DbService.close();
    DbService.connect();
  }

  @Listener('disconnected', { type: 'on' })
  public disconnected() {
    const con = DbService.connect();
    logger.error(`[${con.name.yellow}] ⚠️⚠️⚠️ Connection error ⚠️⚠️⚠️`);
    DbService.close();
    DbService.connect();
  }

  @Listener('reconnected', { type: 'on' })
  public reconnected() {
    const con = DbService.connect();
    logger.error(`[${con.name.yellow}] ⚠️⚠️⚠️ Connection error ⚠️⚠️⚠️`);
  }
}
