import logger from 'services/logger';
import timer from './timer';

export const Log: MethodDecorator = (
  target: Object,
  propertyKey: string | symbol,
  descriptor: TypedPropertyDescriptor<any>,
) => {
  const originalMethod: any = descriptor.value;
  descriptor.value = async function (...args: any[]) {
    let result = null;
    logger.info(`${target.constructor.name}.${propertyKey.toString().yellow} is called`);
    try {
      const ms = await timer(async () => {
        result = await originalMethod.apply(this, args);
      });
      logger.info(
        `${target.constructor.name}.${propertyKey.toString().yellow} is completed, ${`${ms}ms`.yellow}, ${
          'return'.yellow
        } ${JSON.stringify(result)}`,
      );
      return result;
    } catch (e) {
      logger.error(e);
      throw e;
    }
  };
  return descriptor;
};
