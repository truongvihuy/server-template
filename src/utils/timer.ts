const timer = async (func: Function) => {
  const startTime = Date.now();
  await func();
  const endTime = Date.now();
  return endTime - startTime;
};

export default timer;
