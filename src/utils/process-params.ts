import _ from 'lodash';
import { FilterQuery, QueryOptions } from 'mongoose';

export interface QueryField {
  field: string;
  skip: number;
  limit: number;
  sort: number;
}

export function processParams<T = any>(query: any, mapFilter: any): [FilterQuery<T>, any, QueryOptions | null] {
  let filter: FilterQuery<T> = {};
  let projection: any = null;
  let options: QueryOptions | null = null;

  Object.keys(query).forEach((key) => {
    if (mapFilter[key]) {
      mapFilter[key](filter, projection, options);
      return;
    }
    if (!query[key]) {
      return;
    }
    switch (key) {
      case 'field': {
        projection = query.field.split(',');
        break;
      }
      case 'skip':
      case 'limit': {
        if (isNaN(+query[key])) {
          throw Error(`param '${key}' is number`);
        }
        if (!options) {
          options = {};
        }
        options[key] = +query[key];
        break;
      }
      case 'sort': {
        if (!options) {
          options = {};
        }

        const arraySortList = query.sort.split(',');
        for (let e of arraySortList) {
          let [key, order] = e.split(':');
          options = _.merge(options, {
            sort: {
              [key]: 'desc' === order.toLowerCase() ? 1 : -1,
            },
          });
        }
        break;
      }
      default: {
        throw Error(`does not support '${key}' params search.`);
      }
    }
  });

  return [filter, projection, options];
}
