require('dotenv').config();
const package = require('./package.json');

// Visit https://pm2.keymetrics.io/docs/usage/application-declaration/ to read more about this file
module.exports = {
  apps: [
    {
      name: package.name,
      script: package.main,
      watch: ['src'],
      instance_var: 'INSTANCE_ID',
      instances: 'max',
      interpreter_args: `-r dotenv/config -r ts-node/register/transpile-only -r tsconfig-paths/register`,
      env: {
        // config server
        NODE_ENV: process.env.NODE_ENV,
        PORT: +process.env.PORT,
        API_ENDPOINT: process.env.API_ENDPOINT,
        SECRET: process.env.SECRET,
        FOLDER_LOG: process.env.FOLDER_LOG,

        // config mongodb
        DB_CONNECTION_STRING: process.env.DB_CONNECTION_STRING,
      },
    },
  ],
}